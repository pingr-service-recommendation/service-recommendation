const stan = require("node-nats-streaming");

module.exports = function (mongoClient) {
  const conn = stan.connect("test-cluster", "test", {
    url: process.env.BROKER_URL,
  });

  conn.on("connect", () => {
    console.log("Connected to NATS Streaming");
    const db = mongoClient.db("tags")
    const opts = conn.subscriptionOptions().setStartWithLastReceived();
    const subscription = conn.subscribe("pings", opts);
    
    subscription.on("message", async (msg) => {
        const msgData = msg.getData();
        const parsedData = JSON.parse(msgData);
        const { id, tags, date } = parsedData;
        console.log(`[Nats] Received message: ${msgData}`)
        try {
            const pings = db.collection("pings");
            if (await pings.findOne({ id }) === null) {
              await pings.insertOne({ id,  tags, date });
            }
        }
        catch(error) {
            console.log(error);
        }
    });
  });
  
  
  return conn;
};
