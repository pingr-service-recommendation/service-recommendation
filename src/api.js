const jwt = require("jsonwebtoken");
const express = require("express");
const cors = require("cors");
const { v4: uuid } = require("uuid");

module.exports = function (corsOptions, { stanConn, mongoClient, secret }) {
  const api = express();
  const db = mongoClient.db("tags");

  api.use(express.json());
  api.use(cors(corsOptions));

  api.get("/", (req, res) => res.json("Hello, World!"));

  api.get("/tagnow", async (req, res) => {
    const pings = db.collection("pings");

    const staleTime = Date.now() - process.env.TAGS_FREQUENCY_STALE_TIME_IN_MILLISECONDS;
    const tagList =  await pings.aggregate([
      { $match: { date: { $gte: staleTime }}},
      { $project: { tags: true }},
      { $unwind: "$tags"},
      { $group: { _id: "$tags", count: { $sum: 1 }}}
    ]).toArray();

    res.json({
      tags: tagList,
      lastTime: Date.now()
    });
  });

  return api;
};
