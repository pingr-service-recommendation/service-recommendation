const stan = require("node-nats-streaming");

const conn = stan.connect("test-cluster", "nats-pub-client", {
    url: `nats://broker:4222`,
});


const publishPing = () => {
    const message = {
        id: Math.floor(Math.random() * 100000),
        tags: ['a', 'b'],
        date: Date.now()
    };

    conn.publish('pings', JSON.stringify(message), (err, guid) => {
        if (err) {
            console.log('Publish failed: ' + err);
        } else {
            console.log('Published message with guid: ' + guid);
        }
        process.exit();
    });
};

conn.on("connect", () => {
    publishPing();
    console.log("Connected to NATS Streaming");
});

